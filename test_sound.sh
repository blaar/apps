# Basic Libraries And Applications for Robotics (BLAAR)
# Copyright  ETIS — ENSEA, University of Cergy-Pontoise, CNRS (2011 - 2017)
# Author: Arnaud Blanchard
#
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.

#!/usr/bin/env bash #Use your $PATH to find bash
set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but not defined
set -o pipefail #Error if a pipe fail

#We add the bin directory in PATH.
cd $(dirname $0)/../bin
PATH=$PATH:$PWD
cd - >/dev/null #we go back in current directory

#
# Display sound spectrum and emit a sound at the choosen frequency ( default 'la' 440Hz)
#

[ $# = 1 ] && frequency=$1 || frequency=440


#|o_gnuplot --max=0.1  --xmax=200 | #Last pipe make the following line quit at the same time

#size=4096 and refresh=92 ms means that each 92ms a buffer of 4096 values will be filled with the frequency.
#This is suitable for sound samplerate of 44100 (4096/44100Hz = 93ms)

samplerate=44100
buffer=4096

packet_ms=$(echo "$buffer/$samplerate*1000" | bc -l)

limit=$(echo $samplerate/2|bc -l)

i_oscillator --frequency=$frequency --size=$buffer --refresh=$packet_ms| o_coreaudio |
i_coreaudio | f_fftw --spectrum -o.spectrum  | o_gnuplot -t"xlabel 'frequecies (Hz)'" -L$limit -M0.1 --xmax=5000



#clean blc_channels
blc_channels --unlink_unused
